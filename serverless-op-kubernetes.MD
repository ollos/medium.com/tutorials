# Serverless op Kubernetes

Dit is eigenlijk een korte introductie voor het gebruik van Knative en tools hierom heen die we ook binnen 

 [Ollos.IO](https://www.ollos.io) gebruiken en aanbieden voor onze klanten. De bedoeling is in ieder geval om te laten zien wat serverless nu in houdt en in dit geval in combinatie met Knative.

## Serverless

De term serverless komt steeds vaker voor, voornamelijk onder namen (die eigenlijk de lading meer dekken) als Functions, Functions-as-a-Service, of zoals bij AWS, Lambda's. 

Alhoewel de term suggereerd dat er geen servers meer aan te pas komen, klopt dit niet echt. Op de achtergrond zijn er natuurlijk nog steeds machines actief om jouw code uit te voeren. Het verschil zit hem er vooral in dat dit volledig geabstraheerd is. 

Feitelijk gezien houdt dit in dat je zelf geen servers meer in de lucht hoeft te brengen en te onderhouden en dat je code niet meer "geinstalleerd" hoeft te worden. Je schrijft je code en stuurd het naar een FaaS platform, waarna deze meteen actief is. Bekende voorbeelden hiervan zijn AWS Lambda en Azure Functions. Echter zijn er nu ook oplossingen die binnen Kubernetes hetzelfde kunnen doen waarbij je nog meer controle hebt over hoe je je code wilt draaien. Bekende voorbeelden hiervan zijn dan weer OpenWhisk en Knative.

Het idee is dat je je code opknipt in verschillende stukjes, een event (de trigger) en je "functie". Zodra er dus een trigger plaats vind word je functie uitgevoerd (al dan niet met variabelen die meegegeven worden aan je trigger).

Stel je zou een app hebben die de lokale tijd van een land opvraagt zou je dus het volgende kunnen doen:

```pseudocode
(Psuedo)
Trigger: Een URL pad naar: /localtime/japan

Functie:  return datetime.now(japan)
```

In bovenstaand geval is er dus een Trigger die luisterd op /localtime/$country en een functie die de tijd teruggeeft van $country.

Dit kan natuurlijk veel verder gaan, triggers op basis van pub/subs, tijd, wat dan ook. 

Een van de voordelen hiervan is dat je code niet continue hoeft te draaien maar pas bij een event word gestart en meteen weer sluit zodra deze klaar is.

## Kubernetes

Is eigenlijk de defacto container manager. Een platform initieel ontwikkeld door Google en later opensource gemaakt. Zie het als de abstractie laag tussen de containers en de infrastructuur hieronder. 

Het platform zorgt er voor dat je containers blijven draaien, ook als een node wegvalt. Daarnaast regelt het zaken als Loadbalancers, netwerken en service discovery. Natuurlijk doet het nog veel meer maar daarover is het mischien handiger om de officiele site van Kubernetes zelf te lezen.

## Helm

Helm is een veelgebruikt mechanisme om applicatie's binnen kubernetes te beheren. Je configureerd er je applicaties, loadbalancers, secrets, etc. in. Het idee er achter is om deze configuratie's eenvoudig met versie's op te slaan, te delen en hierdoor eenvoudig updates uit te voeren. Zie het als een soort van package manager voor Kubernetes.

## Service Mesh

Een service mesh zou je kunnen zien als de beheerlaag van de services die je draait. Eigenlijk is het een combinatie van tools die op een bepaalde manier je services met elkaar verbindt. Een bekende Mesh is die van [Istio](https://istio.io) alhoewel deze niet alleen voor Kubernetes is is de integratie hiervan wel top geregeld, en word bijvoorbeeld ook aangeboden bij Google.

Een service mesh bestaat meestal uit een paar componenten, in het geval van Istio gaat het om het volgende:

- Connectiviteit
- Veiligheid
- Controle
- Observatie

Dit betekent dat Istio zorgt voor het interne verkeer tussen de individuele diensten, verkeer tussen deze diensten kan versluiteld worden, verkeer kan dusdanig gebalanceerd worden zodat zaken als A/B testing, Canary release etc. eenvoudig toegepast kunnen worden maar ook kan er gemonitord worden TUSSEN deze diensten, zodat er een betere inzicht bestaat over het reilen en zeilen van je applicatie's.

## Knative

En tot slot voor deze tutorial, Knative. Dit is eigenlijk de Kubernetes variant om daadwerkelijk een FaaS (Function-as-a-Service) toe te passen. Het zorgt ervoor dat je code (de functie) word uitgevoerd zodra er een trigger plaats vindt. Het voordeel hierbij is ook dat het via containers word uitgevoerd. Dit houd in dat Knative zelf geen weet hoeft te hebben van de daadwerkelijke taal die gebruikt word.

# Hello World tutorial



Nu we weten wat de bovenstaande onderdelen zijn is het tijd om een eerste app in de lucht te brengen.

We beginnen met de geduchte hello-world (wees niet bang later komt er een interessanter voorbeeld) die we gaan bouwen en live gaan brengen.



## De docker image

Allereerst zullen we een docker image moeten bouwen met een app er in die dus je IP adres teruggeeft.

In dit geval gaat het om een simpel python script:

------

Het python script zelf: app.py:

```python
import os

from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/ip')
def get_ip():
    return jsonify({'ip': request.remote_addr,
                    'user': request.remote_user}), 200


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))

```

------

Een requirements file (om via pip libraries te installeren):

requirements.txt:

```
Flask
gunicorn
```

------

Een dockerfile om daadwerkelijk je docker image te maken:

```dockerfile
FROM python:3.7

ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . .

RUN pip install -r requirements.txt

CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 app:app
```

------

En een .dockerignore om bepaalde bestanden en mappen te negeren (die betekent dat docker bij bijvoorbeeld een COPY actie deze bestanden/mappen NIET meeneemt):

.dockerignore:

```
# Created by .ignore support plugin (hsz.mobi)
### Python template
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]
*$py.class

# C extensions
*.so

# Distribution / packaging
.Python
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
wheels/
pip-wheel-metadata/
share/python-wheels/
*.egg-info/
.installed.cfg
*.egg
MANIFEST

# Installer logs
pip-log.txt
pip-delete-this-directory.txt

# Flask stuff:
instance/
.webassets-cache

# pyenv
.python-version

# pipenv
#   According to pypa/pipenv#598, it is recommended to include Pipfile.lock in version control.
#   However, in case of collaboration, if having platform-specific dependencies or dependencies
#   having no cross-platform support, pipenv may install dependencies that don't work, or not
#   install all needed dependencies.
#Pipfile.lock

# Environments
.env
.venv
env/
venv/
ENV/
env.bak/
venv.bak/
```



Nu we deze files hebben kunnen we de image gaan bakken, uploaden en daadwerkelijk uitvoeren.

Ik ga er vanuit dat je al een vorm van docker registry hebt, hetzij via hub.docker.com, gcr, quay.io of een andere, je moet iig je images weg kunnen schrijven. Als voorbeeld gebruik ik docker hub met onze eigen public registry

```
docker build . -t ollosio/knative-ollos-tutorial:v1
docker push ollosio/knative-ollos-tutorial:v1
```

Zodra dit gedaan is kunnen we aan de slag om daadwerkelijk de applicatie te draaien, in dit geval een knative service:

service-v1.yaml:

```yaml
apiVersion: serving.knative.dev/v1beta1
kind: Service
metadata:
  name: helloworld
  namespace: default
spec:
  template:
    spec:
      containers:
        # Replace {username} with your actual DockerHub
        - image: docker.io/ollosio/knative-ollos-tutorial:v1
          env:
            - name: TARGET
              value: "v1"
```



Zoals je ziet gebruiken we de CRD's van knative zelf (de apiVersion) dit houdt in dat we nu dus een Service maken op Knative (niet te verwarren dus met een Kubernetes eigen type Service).

Om deze uit te voeren:

```
kubectl apply -f service-v1.yaml
```

Zodra dit gedaan is hebben we eigenlijk ons eerste serverless app draaiend :smile: .

Voordat we echter verder gaan moeten we nog wel iets anders checken. Bij het installeren van Istio kregen we een "ingress-gateway" met een IP adres, echter om daadwerkelijk apps aan te bieden op dit IP word er gebruik gemaakt van zogeheten "Host headers". Dit houd in dat de Ingress op basis van je Host weet naar welke service jouw request gerouteerd moet worden. We zullen dus moeten achterhalen waar onze hello-world nu naar luisterd:

```
mallebabbe@LAPTOP-K8LTJNQB:~$ kubectl get route
NAME         URL                                     READY   REASON
helloworld   http://helloworld.default.example.com   True
```

Bovenstaande URL betekend dat als wij een Host header mee sturen naar 'helloworld.default.example.com' we dus worden gerouteerd naar de helloworld Service.

Laten we nog een keer kijken wat ons IP adres is (ik ga er even vanuit dat je dit IP niet genoteerd had bij het installeren van Istio :smile: ):

```
mallebabbe@LAPTOP-K8LTJNQB:~$ kubectl get svc istio-ingressgateway -n istio-system -o jsonpath='{.status.loadBalancer.ingress[0].ip}'
34.90.40.78
```

Als je nu een curl zou doen naar puur het IP zal je een 404 terugkrijgen:

```
mallebabbe@LAPTOP-K8LTJNQB:~/Customers/Ollos/knative/istio$ curl -kLiv http://34.90.40.78
* Rebuilt URL to: http://34.90.40.78/                                                   
*   Trying 34.90.40.78...                                                               
* TCP_NODELAY set                                                                       
* Connected to 34.90.40.78 (34.90.40.78) port 80 (#0)                                   
> GET / HTTP/1.1                                                                        
> Host: 34.90.40.78                                                                     
> User-Agent: curl/7.58.0                                                               
> Accept: */*                                                                           
>                                                                                       
< HTTP/1.1 404 Not Found                                                                
HTTP/1.1 404 Not Found                                                                  
< date: Sun, 28 Jul 2019 09:10:38 GMT                                                   
date: Sun, 28 Jul 2019 09:10:38 GMT                                                     
< server: istio-envoy                                                                   
server: istio-envoy                                                                     
< content-length: 0                                                                     
content-length: 0                                                                       
                                                                                        
<                                                                                       
* Connection #0 to host 34.90.40.78 left intact                                         
```

 We zullen dus het volgende moeten doen:

```
kmeinster@LAPTOP-K8LTJNQB:~/Customers/Ollos/knative/istio$ curl -kLiv http://34.90.40.78 --header 'Host: helloworld.default.example.com'
* Rebuilt URL to: http://34.90.40.78/
*   Trying 34.90.40.78...
* TCP_NODELAY set
* Connected to 34.90.40.78 (34.90.40.78) port 80 (#0)
> GET / HTTP/1.1
> Host: helloworld.default.example.com
> User-Agent: curl/7.58.0
> Accept: */*
>
< HTTP/1.1 200 OK
HTTP/1.1 200 OK
< content-length: 12
content-length: 12
< content-type: text/html; charset=utf-8
content-type: text/html; charset=utf-8
< date: Sun, 28 Jul 2019 09:14:14 GMT
date: Sun, 28 Jul 2019 09:14:14 GMT
< server: istio-envoy
server: istio-envoy
< x-envoy-upstream-service-time: 8384
x-envoy-upstream-service-time: 8384

<
* Connection #0 to host 34.90.40.78 left intact
Hello World!
```

Met een beetje gelukt merk je dat de request nogal...erm...langzaam was. De reden hiervoor is eenvoudig, de applicatie word dynamisch opgestart zodra er een request voor binnen komt. Zodra er een tijdje geen request heeft plaats gevonden zal de pod stoppen met draaien. Zodra de pod dus weer een keer opgestart word, zal eerst de docker image gedownload worden om daarna gestart te worden...En ja...later zul je zien hoe dit te voorkomen (en waneer je dit zou moeten doen).